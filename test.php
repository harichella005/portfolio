<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Offcanvas</title>
    <script src="index.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <input type="checkbox" id="toggleCheckbox">
    <label for="toggleCheckbox" class="toggle-label">
        <i id="hamburger" class="fas fa-bars"></i>
    </label>

    <div class="offcanvas">
        <label for="toggleCheckbox" class="toggle-label">
            <i class="fas fa-times"></i>
        </label>
        <!-- Offcanvas content here -->
        <h3>Offcanvas</h3>
    </div>



</body>

</html>