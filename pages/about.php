<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>About Me - Hariharan
    </title>
    <link rel=" stylesheet" href="../css/about.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="index.js"></script>
</head>

<body>
    <header id="fixed-header">
        <nav>
            <div class="nav-left">
                <img src="../images/hari.png" alt="">
            </div>
            <div class="nav-mid">
                <h1>Hariharan Chellamuthu</h1>
            </div>
            <input type="checkbox" id="toggleCheckbox">
            <label for="toggleCheckbox" class="toggle-label">
                <i id="hamburger" class="fas fa-bars"></i>
            </label>

            <div class="offcanvas">
                <label for="toggleCheckbox" class="toggle-label">
                    <i class="fas fa-times"></i>
                </label>
                <!-- Offcanvas content here -->
                <h3>Menu</h3>
                <ul class="nav-links">
                    <li><a href="/" active>Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/skills">Skills</a></li>
                    <li><a href="#projects">Projects</a></li>
                    <li><a href="#experience">Experience</a></li>
                    <li><a href="#education">Education</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <hr class="horizontal">

    <div class="aboutexp">
        <h3>About Me</h3>
        <h4>6 Years of Experience</h4>
        <hr class="horizontal">
    </div>
    <section id="about">
        <div class="shortbio">
            <p>I'm Hariharan, a Computer Science Engineering graduate
                from Dhanalakshmi Srinivasan College of Engineering and
                Technology. During my college years,
                I developed a strong interest in programming languages
                and computer basics. This led me to discover the world
                of bug hunting, which I enthusiastically
                pursued by researching extensively and gaining expertise
                in identifying and fixing bugs across web applications,
                Android systems, and APIs.
            </p>
            <p>I have honed my skills in various programming languages,
                including Python, Bash, PHP, HTML, CSS, JavaScript, C,
                C++, Golang, and SQL. Additionally,
                I am well-versed in front-end frameworks like ReactJS,
                Redux, and responsive design. My curiosity and eagerness
                to explore new realms led me to delve into
                electronics, where I gained hands-on experience with
                Arduino and Raspberry Pi, completing several successful
                projects.
            </p>
            <p>In parallel to my passion for programming and
                electronics, I am deeply committed to cybersecurity. I
                derive immense satisfaction from ensuring the security
                of
                digital systems and safeguarding sensitive data from
                potential threats. Throughout my journey, I have had the
                privilege of working with renowned companies like
                LifeOmic,
                Courier, Centrify, Zulip, and Trip Advisor. These
                experiences provided me with valuable insights into the
                intricacies of cybersecurity in diverse industries.
            </p>
            <p>
                They allowed me to analyze vulnerabilities, develop
                robust security solutions, and contribute to the
                confidentiality, integrity, and availability of critical
                data.
                To stay at the forefront of cybersecurity, I
                continuously update my knowledge and skills, embracing
                the latest trends and techniques. My dedication to this
                field
                drives me to explore new avenues, tackle complex
                security issues, and actively contribute to creating a
                safer digital environment.
            </p>
            <br>
        </div>
        <br>
        <br>
    </section>
    <section id="progress">
        <div class="programming-lang">
            <h2>Programming Languages</h2>
            <div class="program">
                <h4 class="lang">C</h4>
                <h4 class="value">90%</h4>
                <progress value="90" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">C++</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">PHP</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">JAVA</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
        </div>
        <div class="programming-lang">
            <h2>Application Development</h2>
            <div class="program">
                <h4 class="lang">Android</h4>
                <h4 class="value">90%</h4>
                <progress value="90" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">CLI and GUI</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">Desktop Softwares (Windows and Linux)</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">Web Application</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
        </div>
        <div class="programming-lang">
            <h2>Database Skills</h2>
            <div class="program">
                <h4 class="lang">SQL</h4>
                <h4 class="value">90%</h4>
                <progress value="90" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">MongoDB</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">Firebase</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
            <div class="program">
                <h4 class="lang">Web Application</h4>
                <h4 class="value">80%</h4>
                <progress value="80" max="100"></progress>
            </div>
        </div>
    </section>
    <section id="work-experience">
        <h1>Work Experience</h1>
        <div class="companyinfo">
            <img src="../images/hari.png" alt="" class="">
            <h2>Skill-lync - Full-time</h2>
            <h3>Sep 2021 - Jun 2023</h3>
            <h4>Chennai, Tamilnadu, India</h4>
        </div>
    </section>
    
    <footer>
        <div class="social-icons">
            <a href="#" class="icon-link">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/in/hariharan-c-0186531aa/" class="icon-link">
                <i class="fab fa-linkedin"></i>
            </a>
        </div>
        <div class="copyrights">
            <p>&copy; 2023 All rights reserved </p>
        </div>
    </footer>
</body>

</html>