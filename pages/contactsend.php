<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $firstName = $_POST["first-name"];
    $lastName = $_POST["last-name"];
    $email = $_POST["email"];
    $contact = $_POST["contact"];
    $message = $_POST["message"];

    // Combine the form data as a string for the text file
    $formData = "First Name: " . $firstName . "\n" .
                "Last Name: " . $lastName . "\n" .
                "Email: " . $email . "\n" .
                "Contact: " . $contact . "\n" .
                "Message: " . $message . "\n\n";


    // Get user IP address
    $userIP = $_SERVER["REMOTE_ADDR"];

    // Get user agent information
    $userAgent = $_SERVER["HTTP_USER_AGENT"];

    // Append user IP and user agent to the text file data
    $formData .= "User IP: " . $userIP . "\n";
    $formData .= "User Agent: " . $userAgent . "\n\n";

    
    // Open the file in append mode and write the form data
    $fileText = fopen("form_data.txt", "a");
    fwrite($fileText, $formData);
    fclose($fileText);

    //Create an associalte array with form data for the JSON file
    $formDataArray = array(
        "First Name" => $firstName,
        "Last Name" => $lastName,
        "Email" => $email,
        "Contact" => $contact,
        "Message" => $message,
        "User IP" => $userIP,
        "User Agent" => $userAgent
    );

    // Convert the associative array to JSON

    $formDataJson = json_encode($formDataArray, JSON_PRETTY_PRINT);

    // Open the JSON file in append mode and write the Json data
    $fileJson = fopen("form_data.json", "a");
    fwrite($fileJson, $formData);
    fclose($fileJson);

    // Redirect to a "thank you" page or another appropriate page
    header("Location: thanks.html");
    exit;
}
