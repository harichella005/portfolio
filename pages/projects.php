<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Hariharan - Projects</title>
    <link rel="stylesheet" href="./css/projects.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="index.js"></script>
    <link rel="shortcut icon" href="../images/hari.ico" type="image/x-icon">
</head>

<body>
    <header id="fixed-header">
        <nav>
            <div class="nav-left">
                <img src="../images/hari.png" alt="">
            </div>
            <div class="nav-mid">
                <h1>Hariharan Chellamuthu</h1>
            </div>
            <input type="checkbox" id="toggleCheckbox">
            <label for="toggleCheckbox" class="toggle-label">
                <i id="hamburger" class="fas fa-bars"></i>
            </label>

            <div class="offcanvas">
                <label for="toggleCheckbox" class="toggle-label">
                    <i class="fas fa-times"></i>
                </label>
                <!-- Offcanvas content here -->
                <h3>Menu</h3>
                <ul class="nav-links">
                    <li><a href="/" active>Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/skills">Skills</a></li>
                    <li><a href="/projects">Projects</a></li>
                    <li><a href="/experience">Experience</a></li>
                    <li><a href="/education">Education</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </header>


    <!-- This is project section  -->


  


    <div class="row">

        <div class="columns">
            <a target="_blank" href="https://github.com/hariharan005/CORS">
                <img class="project-img" src="../images/cors.gif" alt="">
                <p class="short-paragraph">The CORS - Analyzer cli tool is designed to identify vulnerable Cross-Origin Resource Sharing (CORS) configurations in web applications. CORS misconfigurations can lead to security vulnerabilities, allowing unauthorized access to sensitive information or enabling malicious activities. This tool offers a feature that allows simultaneous identification of multiple domains, which can be beneficial for both developers and bug hunters. Developers can use the tool to scan their own applications and ensure that their CORS configurations are secure. Bug hunters, on the other hand, can leverage this tool to identify vulnerable CORS domains in web applications they are testing, helping them discover and report security vulnerabilities. By using the CORS tool, developers and bug hunters can proactively address CORS misconfigurations, strengthening the security of web applications and protecting against potential attacks.</p>
            </a>
        </div>
        <div class="columns">
            <a target="_blank" href="https://gitlab.com/harichella005/subdomain-seeker">
                <img class="project-img" src="../images/Subdomian-seeker.gif" alt="">
                <p class="short-paragraph">"Subdomain Seeker" is a Go programming language tool designed to discover subdomains associated with a domain or website. It utilizes advanced Go features like goroutines for efficient scanning. This tool aids in security testing by identifying potential vulnerabilities or entry points through subdomains.</p>
            </a>
        </div>
        <div class="columns">
            <a target="_blank" href="https://github.com/hariharan005/CORS">
                <img class="project-img" src="../images/cors.gif" alt="">
                <p class="short-paragraph">The CORS - Analyzer cli tool is designed to identify vulnerable Cross-Origin Resource Sharing (CORS) configurations in web applications. CORS misconfigurations can lead to security vulnerabilities, allowing unauthorized access to sensitive information or enabling malicious activities. This tool offers a feature that allows simultaneous identification of multiple domains, which can be beneficial for both developers and bug hunters. Developers can use the tool to scan their own applications and ensure that their CORS configurations are secure. Bug hunters, on the other hand, can leverage this tool to identify vulnerable CORS domains in web applications they are testing, helping them discover and report security vulnerabilities. By using the CORS tool, developers and bug hunters can proactively address CORS misconfigurations, strengthening the security of web applications and protecting against potential attacks.</p>
            </a>
        </div>
        <div class="columns">
            <a target="_blank" href="https://github.com/hariharan005/CORS">
                <img class="project-img" src="../images/cors.gif" alt="">
                <p class="short-paragraph">The CORS - Analyzer cli tool is designed to identify vulnerable Cross-Origin Resource Sharing (CORS) configurations in web applications. CORS misconfigurations can lead to security vulnerabilities, allowing unauthorized access to sensitive information or enabling malicious activities. This tool offers a feature that allows simultaneous identification of multiple domains, which can be beneficial for both developers and bug hunters. Developers can use the tool to scan their own applications and ensure that their CORS configurations are secure. Bug hunters, on the other hand, can leverage this tool to identify vulnerable CORS domains in web applications they are testing, helping them discover and report security vulnerabilities. By using the CORS tool, developers and bug hunters can proactively address CORS misconfigurations, strengthening the security of web applications and protecting against potential attacks.</p>
            </a>
        </div>
        <div class="columns">
            <a target="_blank" href="https://github.com/hariharan005/CORS">
                <img class="project-img" src="../images/cors.gif" alt="">
                <p class="short-paragraph">The CORS - Analyzer cli tool is designed to identify vulnerable Cross-Origin Resource Sharing (CORS) configurations in web applications. CORS misconfigurations can lead to security vulnerabilities, allowing unauthorized access to sensitive information or enabling malicious activities. This tool offers a feature that allows simultaneous identification of multiple domains, which can be beneficial for both developers and bug hunters. Developers can use the tool to scan their own applications and ensure that their CORS configurations are secure. Bug hunters, on the other hand, can leverage this tool to identify vulnerable CORS domains in web applications they are testing, helping them discover and report security vulnerabilities. By using the CORS tool, developers and bug hunters can proactively address CORS misconfigurations, strengthening the security of web applications and protecting against potential attacks.</p>
            </a>
        </div>
        <div class="columns">
            <a target="_blank" href="https://github.com/hariharan005/CORS">
                <img class="project-img" src="../images/cors.gif" alt="">
                <p class="short-paragraph">The CORS - Analyzer cli tool is designed to identify vulnerable Cross-Origin Resource Sharing (CORS) configurations in web applications. CORS misconfigurations can lead to security vulnerabilities, allowing unauthorized access to sensitive information or enabling malicious activities. This tool offers a feature that allows simultaneous identification of multiple domains, which can be beneficial for both developers and bug hunters. Developers can use the tool to scan their own applications and ensure that their CORS configurations are secure. Bug hunters, on the other hand, can leverage this tool to identify vulnerable CORS domains in web applications they are testing, helping them discover and report security vulnerabilities. By using the CORS tool, developers and bug hunters can proactively address CORS misconfigurations, strengthening the security of web applications and protecting against potential attacks.</p>
            </a>
        </div>
        
    </div>

    <!-- This is footer contents -->
    <footer>
        <div class="social-icons">
            <a href="#" class="icon-link">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/in/hariharan-c-0186531aa/" class="icon-link">
                <i class="fab fa-linkedin"></i>
            </a>
            <a target="_blank" href="https://hariharanchellamuthu.medium.com/" class="icon-link">
                <i class="fab fa-medium"></i>
            </a>
        </div>
        <div class="copyrights">
            <p>&copy; 2023 All rights reserved </p>
        </div>
    </footer>
</body>

</html>