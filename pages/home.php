<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Hariharan - Cybersecurity enthusiast skilled in bug hunting,
        programming, and electronics.
    </title>
    <link rel="shortcut icon" href="../images/hari.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="index.js"></script>
</head>

<body>
    <header id="fixed-header">
        <nav>
            <div class="nav-left">
                <img src="../images/hari.png" alt="">
            </div>
            <div class="nav-mid">
                <h1>Hariharan Chellamuthu</h1>
            </div>
            <input type="checkbox" id="toggleCheckbox">
            <label for="toggleCheckbox" class="toggle-label">
                <i id="hamburger" class="fas fa-bars"></i>
            </label>

            <div class="offcanvas">
                <label for="toggleCheckbox" class="toggle-label">
                    <i class="fas fa-times"></i>
                </label>
                <!-- Offcanvas content part  -->
                <h3>Menu</h3>
                <ul class="nav-links">
                    <li><a href="/" active>Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/skills">Skills</a></li>
                    <li><a href="/projects">Projects</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <div class="main">
        <section id="home">
            <div class="profile">
                <img src="../images/hariharan.jpg" alt="">
            </div>
            <header class="role">
                <h1>I'm</h1>
                <h2 id="title">Web Developer</h2>
            </header>
            <div class="shortbio">
                <p>I'm Hariharan, a Computer Science Engineering graduate
                    from Dhanalakshmi Srinivasan College of Engineering and
                    Technology. During my college years,
                    I developed a strong interest in programming languages
                    and computer basics. This led me to discover the world
                    of bug hunting, which I enthusiastically
                    pursued by researching extensively and gaining expertise
                    in identifying and fixing bugs across web applications,
                    Android systems, and APIs.
                </p>
                <br>
            </div>
            <div class="knowmore-btn">
                <a href="/about">Know More</a>
            </div>
            <br>
            <br>
        </section>
    </div>

    <footer>
        <div class="social-icons">
            <a href="#" class="icon-link">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/in/hariharan-c-0186531aa/" class="icon-link">
                <i class="fab fa-linkedin"></i>
            </a>
        </div>
        <div class="copyrights">
            <p>&copy; 2023 All rights reserved </p>
        </div>
    </footer>
</body>

</html>