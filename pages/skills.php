<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Skills - Hariharan
    </title>
    <link rel="stylesheet" href="../css/skills.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <script src="index.js"></script>
</head>

<body>
    <header id="fixed-header">
        <nav>
            <div class="nav-left">
                <img src="../images/hari.png" alt="">
            </div>
            <div class="nav-mid">
                <h1>Hariharan Chellamuthu</h1>
            </div>
            <input type="checkbox" id="toggleCheckbox">
            <label for="toggleCheckbox" class="toggle-label">
                <i id="hamburger" class="fas fa-bars"></i>
            </label>

            <div class="offcanvas">
                <label for="toggleCheckbox" class="toggle-label">
                    <i class="fas fa-times"></i>
                </label>
                <!-- Offcanvas content here -->
                <h3>Menu</h3>
                <ul class="nav-links">
                    <li><a href="/" active>Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="#skills">Skills</a></li>
                    <li><a href="#projects">Projects</a></li>
                    <li><a href="#experience">Experience</a></li>
                    <li><a href="#education">Education</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
        </nav>
    </header>

    <section id="skills">
        <div class="programming-lang">
            <h2>Programming Languages</h2>
            <div class="skill">
                <h4>C <span>
                        <p class="skill-percentage">80%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 80%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>C++ <span>
                        <p class="skill-percentage">70%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 70%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>PHP <span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Java <span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Python<span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Golang <span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>JavaScript<span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
        </div>

        <!-- The application development div starts here -->
        <div class="application-dev">
            <h2>Application Developement</h2>
            <div class="skill">
                <h4>Android <span>
                        <p class="skill-percentage">80%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 80%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>CLI and GUI <span>
                        <p class="skill-percentage">70%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 70%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Desktop Softwares (Windows and Linux)<span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Web Application <span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
        </div>

        <!-- The Database Skills div starts here -->
        <div class="application-dev">
            <h2>Database Skills</h2>
            <div class="skill">
                <h4>MySQL <span>
                        <p class="skill-percentage">80%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 80%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>Firebase <span>
                        <p class="skill-percentage">70%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 70%;">

                    </div>
                </div>
            </div>
            <div class="skill">
                <h4>MongoDB<span>
                        <p class="skill-percentage">90%</p>
                    </span></h4>
                <div class="progress-bar">
                    <div class="progress" style="width: 90%;">

                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="social-icons">
            <a href="#" class="icon-link">
                <i class="fab fa-facebook"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="#" class="icon-link">
                <i class="fab fa-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/in/hariharan-c-0186531aa/" class="icon-link">
                <i class="fab fa-linkedin"></i>
            </a>
        </div>
        <div class="copyrights">
            <p>&copy; 2023 All rights reserved </p>
        </div>
    </footer>
</body>

</html>