<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $username = $_POST['username'];
    $email = $_POST['email'];

    setcookie('username', $username, time() + 60);
    setcookie('email', $email, time() + 60);

    header('Location: index.html?message=success');
    exit;
} else {
    header('Location: index.html?message=error');
    exit;
}
?>

